import React, { Component } from 'react';
import { Text, View, AsyncStorage } from 'react-native';
import { CardItem, List, ListItem, Body, Right, Content, Card, Form, Item, Label, Input, Button, Icon, Picker } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import LumpSumService from './../Services/LumpSumService';
import Number from './../Services/Number';

class LumpsumCalculator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            periodType: "year",
            amount: null,
            percentage: null,
            period: null,
            result: null,
            currency: null
        };

        this.handleChange = this.handleChange.bind(this);
        this.calculateLumpSum = this.calculateLumpSum.bind(this);
        this.formatAmount = this.formatAmount.bind(this);
    }

    handleChange(property, value) {
        this.setState({
            [property]: value
        });
    }

    formatAmount(amount) {
        return (new Number).format(amount, this.state.currency);
    }

    componentDidMount() {
        var that = this;
        AsyncStorage.getItem('@financeGuru:currency').then((currency) => {
            that.setState({ currency: currency });
        })
    }

    calculateLumpSum() {
        result = (new LumpSumService).calculateLumpSum(
            this.state.amount,
            this.state.percentage,
            this.state.period,
            this.state.periodType
        );

        this.setState({
            result: result
        });
    }

    render() {
        return (
            <Content>
                <Form>
                    <Item stackedLabel>
                        <Label>Lumpsum Investment Amount</Label>
                        <Input
                            value={this.state.amount}
                            keyboardType="decimal-pad"
                            onChangeText={(value) => this.handleChange('amount', value)}
                        />
                    </Item>
                    <Item stackedLabel>
                        <Label>Expected Annual Returns (%)</Label>
                        <Input
                            value={this.state.percentage}
                            keyboardType="decimal-pad"
                            onChangeText={(value) => this.handleChange('percentage', value)}
                        />
                    </Item>
                    <Item stackedLabel>
                        <Label>Investment Period</Label>
                        <Input
                            value={this.state.period}
                            keyboardType="decimal-pad"
                            onChangeText={(value) => this.handleChange('period', value)}
                        />
                    </Item>
                    <Item stackedLabel last picker>
                        <Label>Tenure Type</Label>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: '100%' }}
                            placeholder="Tenure Type"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.periodType}
                            onValueChange={(value) => this.handleChange('periodType', value)}
                        >
                            <Picker.Item label="Year" value="year" />
                            <Picker.Item label="Month" value="month" />
                        </Picker>
                    </Item>
                    <View style={{ padding: 5, marginTop: 5 }}>
                        <Button primary block onPress={() => this.calculateLumpSum()}>
                            <Icon name='calculator' />
                            <Text style={{ color: 'white' }}>Calculate</Text>
                        </Button>
                    </View>
                </Form>
                {this.state.result &&
                    <View>
                        <Card>
                            <CardItem header bordered>
                                <Text>Summery</Text>
                            </CardItem>
                            <CardItem bordered>
                                <Body>
                                    <CardItem>
                                        <Text>Expected Amount</Text>
                                        <Right>
                                            <Text>{this.formatAmount(this.state.result.futureValue)}</Text>
                                        </Right>
                                    </CardItem>
                                    <CardItem>
                                        <Text>Amount Invested</Text>
                                        <Right>
                                            <Text>{this.formatAmount(this.state.result.investedValue)}</Text>
                                        </Right>
                                    </CardItem>
                                    <CardItem>
                                        <Text>Wealth Gain</Text>
                                        <Right>
                                            <Text>{this.formatAmount(this.state.result.futureValue - this.state.result.investedValue)}</Text>
                                        </Right>
                                    </CardItem>
                                </Body>
                            </CardItem>
                        </Card>
                        <Card>
                            <CardItem header bordered>
                                <Text>Projected Lumpsum returns for various time durations.</Text>
                            </CardItem>
                            <CardItem>
                                <Grid>
                                    <Row>
                                        <Col><Text style={{ fontWeight: '600' }}>Duration</Text></Col>
                                        <Col><Text style={{ fontWeight: '600' }}>Invested Amount</Text></Col>
                                        <Col><Text style={{ fontWeight: '600' }}>Future Value</Text></Col>
                                    </Row>
                                </Grid>
                            </CardItem>
                            <CardItem bordered>
                                <Body>
                                    {
                                        this.state.result.intervals.map((interval) => (
                                            <CardItem>
                                                <Grid>
                                                    <Row>
                                                        <Col><Text>{interval.period}</Text></Col>
                                                        <Col><Text>{this.formatAmount(interval.investedValue)}</Text></Col>
                                                        <Col><Text>{this.formatAmount(interval.futureValue)}</Text></Col>
                                                    </Row>
                                                </Grid>
                                            </CardItem>
                                        ))
                                    }
                                </Body>
                            </CardItem>
                        </Card>
                    </View>
                }
            </Content>
        );
    }
}

export default LumpsumCalculator;