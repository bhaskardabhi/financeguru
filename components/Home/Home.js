import React, { Component } from 'react';
import { Text, View, ImageBackground } from 'react-native';
import { Content, Body } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Card, CardItem, Thumbnail, H3, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';

class Home extends Component {
    render() {
        var pathToAsset = "./../../assets/images/";

        var items = [
            { title: 'SIP Calculator', image: require('./../../assets/images/process_round.png'), description: 'Calculate SIP Return', action: 'sipCalculator'},
            { title: 'Lumpsum Calculator', image: require('./../../assets/images/money_bag.png'), description: 'Calculate Lumpsum Return', action: 'lumpsumCalculator' },
            { title: 'Fixed Deposit Calculator', image: require('./../../assets/images/money_umbrella.png'), description: 'Calculate Fixed Deposit Return', action: 'fdCalculator' },
            { title: 'Recurring Deposit Calculator', image: require('./../../assets/images/hand_tree.png'), description: 'Calculate Recurring Deposit Return', action: 'rdCalculator' },
            { title: 'Loan Calculator', image: require('./../../assets/images/house_love.png'), description: 'Calculate EMI, Affordability, Tenure & Interest Rate', action: 'loanCalculator' },
            { title: 'HRA Calculator', image: require('./../../assets/images/home_tax.png'), description: 'Calculate Your House Rent Allowance', action: 'hraCalculator' }
        ];

        var chuckedItems = [];

        items.map(function (item, index){
            var chuckIndex = Math.floor(index / 2);
            if (chuckedItems[chuckIndex] === undefined){
                chuckedItems[chuckIndex] = [];
            }
            
            chuckedItems[chuckIndex].push(item);
        });

        return (
            <Content padder>
                <Grid>
                    {chuckedItems.map((items,chuckIndex) => (
                        <Row key={chuckIndex} style={{ marginBottom: 10 }}>
                            {items.map((item,index) => (
                                <Col key={index} size={25}>
                                    <Card style={{ height: '100%' }}>
                                        <CardItem button onPress={() => Actions[item.action]()}>
                                            <Body style={{justifyContent: "center", alignItems: "center"}}>
                                                <H3 style={{ marginBottom: 10, fontSize: 15,textAlign: 'center'}}>{item.title}</H3>
                                                <Thumbnail style={{ marginBottom: 10 }} source={item.image} />
                                                <Text style={{ fontSize: 10,textAlign: 'center'}}>{item.description}</Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </Col>
                            ))}
                        </Row>
                    ))}
                </Grid>
            </Content>
        );
    }
}

export default Home;