class EMICalculationService {
    calculateEMI(loanAmount, annualRate, period, periodType) {
        that = this;

        if (periodType == 'year') {
            var months = period * 12;
        } else {
            var months = period;
        }

        var result = {
            emi: that.getEMIByParameters(loanAmount, annualRate, months),
        }

        result.principalAmount = loanAmount;
        result.totalPayableAmount = result.emi * months;
        result.interestAmount = result.totalPayableAmount - result.principalAmount;

        return result;
    }

    getEMIByParameters(loanAmount, annualRate, months){
        var monthlyRate = annualRate / 12 / 100;

        // https://www.businesstoday.in/moneytoday/banking/how-to-calculate-emi-on-your-loans-formula-explanation/story/198944.html
        // (2500000 * ((8.60 / 100) / 12) * Math.pow(1 + ((8.60 / 100) / 12), 12 * 20)) / (Math.pow(1 + ((8.60 / 100) / 12), 12 * 20) - 1)
        return (loanAmount * monthlyRate * Math.pow(1 + monthlyRate, months)) / (Math.pow(1 + monthlyRate, months) - 1);
        // return investment * Math.pow(1 + monthlyRate, months);
    }
}

export default EMICalculationService;