class SipService {
    calculateSIP (investment, annualRate, period, periodType) {
        var monthlyRate = annualRate / 12 / 100,
            that = this;

        if (periodType == 'year') {
            var months = period * 12;
        } else {
            var months = period;
        }

        var result = {
            futureValue: that.getSIPByParameters(investment, annualRate, months),
            investedValue: investment * months,
            intervals: []
        }

        if (months < 12) {
            Array.from({ length: months }, (v, k) => k + 1).forEach((month) => {
                result.intervals.push({
                    period: month + ' Month',
                    investedValue: investment * month,
                    futureValue: that.getSIPByParameters(investment, annualRate, month)
                });
            });
        } else {
            var years = Math.floor(months / 12);
            Array.from({ length: years }, (v, k) => k + 1).forEach((year) => {
                result.intervals.push({
                    period: year + ' Year',
                    investedValue: investment * year * 12,
                    futureValue: that.getSIPByParameters(investment, annualRate, year * 12)
                });
            });

            var remainingMonths = months - (years * 12);

            if (remainingMonths > 0) {
                Array.from({ length: remainingMonths }, (v, k) => k + 1).forEach((month) => {
                    result.intervals.push({
                        period: years + ' Year ' + month + ' Month',
                        investedValue: (investment * years * 12) + (investment * month),
                        futureValue: that.getSIPByParameters(investment, annualRate, ((years*12)+month))
                    });
                });
            }
        }

        return result;
    }

    getSIPByParameters(investment, annualRate, months){
        var monthlyRate = annualRate / 12 / 100;

        return (investment * (1 + monthlyRate) * ((Math.pow((1 + monthlyRate), months)) - 1) / monthlyRate);
    }
}

export default SipService;