class LumpSumService {
    calculateLumpSum (investment, annualRate, period, periodType) {
        that = this;

        if (periodType == 'year') {
            var months = period * 12;
        } else {
            var months = period;
        }

        var result = {
            futureValue: that.getLumpSumByParameters(investment, annualRate, months),
            investedValue: investment,
            intervals: []
        }

        if (months < 12) {
            Array.from({ length: months }, (v, k) => k + 1).forEach((month) => {
                result.intervals.push({
                    period: month + ' Month',
                    investedValue: investment,
                    futureValue: that.getLumpSumByParameters(investment, annualRate, month)
                });
            });
        } else {
            var years = Math.floor(months / 12);
            Array.from({ length: years }, (v, k) => k + 1).forEach((year) => {
                result.intervals.push({
                    period: year + ' Year',
                    investedValue: investment,
                    futureValue: that.getLumpSumByParameters(investment, annualRate, year * 12)
                });
            });

            var remainingMonths = months - (years * 12);

            if (remainingMonths > 0) {
                Array.from({ length: remainingMonths }, (v, k) => k + 1).forEach((month) => {
                    result.intervals.push({
                        period: years + ' Year ' + month + ' Month',
                        investedValue: investment,
                        futureValue: that.getLumpSumByParameters(investment, annualRate, ((years*12)+month))
                    });
                });
            }
        }

        return result;
    }

    getLumpSumByParameters(total, ratePercent, months){
        var interestRate = ((ratePercent / 100) + 1),
            years = parseFloat((months / 12).toFixed(2));

        return parseFloat(
            (total * Math.pow(interestRate, years))
        ).toFixed(2);
    }
}

export default LumpSumService;