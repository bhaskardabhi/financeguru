class FDService {
    calculateFD (investment, annualRate, period, periodType) {
        that = this;

        if (periodType == 'year') {
            var months = period * 12;
        } else {
            var months = period;
        }

        var result = {
            futureValue: that.getFDByParameters(investment, annualRate, months),
            investedValue: investment,
            intervals: []
        }

        if (months < 12) {
            Array.from({ length: months }, (v, k) => k + 1).forEach((month) => {
                result.intervals.push({
                    period: month + ' Month',
                    investedValue: investment,
                    futureValue: that.getFDByParameters(investment, annualRate, month)
                });
            });
        } else {
            var years = Math.floor(months / 12);
            Array.from({ length: years }, (v, k) => k + 1).forEach((year) => {
                result.intervals.push({
                    period: year + ' Year',
                    investedValue: investment,
                    futureValue: that.getFDByParameters(investment, annualRate, year * 12)
                });
            });

            var remainingMonths = months - (years * 12);

            if (remainingMonths > 0) {
                Array.from({ length: remainingMonths }, (v, k) => k + 1).forEach((month) => {
                    result.intervals.push({
                        period: years + ' Year ' + month + ' Month',
                        investedValue: investment,
                        futureValue: that.getFDByParameters(investment, annualRate, ((years*12)+month))
                    });
                });
            }
        }

        return result;
    }

    getFDByParameters(investment, annualRate, months){
        var monthlyRate = annualRate / 12 / 100;

        // http://www.allbankingsolutions.com/fdcal.htm
        return investment * Math.pow(1 + monthlyRate, months);
    }
}

export default FDService;