// import { AsyncStorage } from 'react-native';

class Number {
    format = (value, currency) => {
        if(!currency){
            currency = 'rupee';
        }

        try {
            if (currency == 'rupee'){
                return this.formatByInr(value);
            } else {
                return this.formatByUSD(value);
            }
        } catch (error) {
            return value;
        }
    }

    formatByInr = (value) => {
        var val = Math.abs(value);
        if (val >= 10000000) {
            val = (val / 10000000).toFixed(2) + ' Cr';
        } else if (val >= 100000) {
            val = (val / 100000).toFixed(2) + ' Lac';
        } else {
            val = parseFloat(val).toFixed(2);
        }

        return val;
    }

    formatByUSD = (value) => {
        var val = Math.abs(value);
        if (val >= 1000) {
            val = (val / 1000).toFixed(2) + ' Grand';
        } else if (val >= 1000000) {
            val = (val / 1000000).toFixed(2) + ' Mill';
        } else if (val >= 1000000000) {
            val = (val / 1000000000).toFixed(2) + ' Bill';
        } else {
            val = parseFloat(val).toFixed(2);
        }

        return val;
    }
}

export default Number;