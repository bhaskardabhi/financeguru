// 1	Actual HRA received	₹ 1, 00, 000
// 2	50 % of[(45, 000 + 7, 000) * 12]	₹ 3, 12, 000
// 3	Actual rent ₹ 3, 00, 000 minus 10 % of[(45, 000 + 7, 000) * 12]	₹ 2, 37, 600
// 4	HRA deduction = Least of 1, 2, 3

class HRACalculationService {
    calculateHRA(basicSalary, daReceived, hraReceived, totalRentPaid, liveInMetro) {
        var basicSalaryPlusDA = parseFloat(basicSalary) + parseFloat(daReceived),
            percent = liveInMetro ? 50 : 40,
            point1 = hraReceived,
            point2 = (basicSalaryPlusDA * percent)/100, 
            point3 = parseFloat(totalRentPaid) - ((basicSalaryPlusDA*10)/ 100),
            exemptedHRA = Math.min.apply(Math, [
                parseFloat(point1),
                parseFloat(point2),
                parseFloat(point3)
            ]);

        exemptedHRA = exemptedHRA > 0 ? exemptedHRA : 0;

        return {
            exemptedHRA: exemptedHRA,
            hraChargeableToTax: hraReceived > exemptedHRA ? (hraReceived - exemptedHRA) : 0
        };
    }
}

export default HRACalculationService;