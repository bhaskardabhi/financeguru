import React, { Component } from 'react';
import { Text, View, AsyncStorage } from 'react-native';
import { CardItem, List, ListItem, Body, Right, Content, CheckBox, Card, Form, Item, Label, Input, Button, Icon, Picker } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import HRACalculationService from '../Services/HRACalculationService';
import Number from '../Services/Number';

class HRACalculator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            basicSalary: null,
            daReceived: null,
            hraReceived: null,
            totalRentPaid: null,
            liveInMetro: false,
            result: null
        };

        this.handleChange = this.handleChange.bind(this);
        this.calculateHRA = this.calculateHRA.bind(this);
        this.formatAmount = this.formatAmount.bind(this);
    }

    handleChange(property, value) {
        this.setState({
            [property]: value
        });
    }

    formatAmount(amount) {
        return (new Number).format(amount, this.state.currency);
    }

    calculateHRA() {
        if (!this.state.basicSalary){
            alert("Please Enter Basic Salary");
            return true;
        }

        if (!this.state.daReceived) {
            alert("Please Enter Dearness Allowance (DA) received");
            return true;
        }

        if (!this.state.hraReceived) {
            alert("Please Enter HRA received");
            return true;
        }

        if (!this.state.totalRentPaid) {
            alert("Please Enter Total Rent Paid");
            return true;
        }

        this.setState({
            result: (new HRACalculationService).calculateHRA(
                this.state.basicSalary,
                this.state.daReceived,
                this.state.hraReceived,
                this.state.totalRentPaid,
                this.state.liveInMetro
            )
        });
    }

    componentDidMount() {
        var that = this;
        AsyncStorage.getItem('@financeGuru:currency').then((currency) => {
            that.setState({ currency: currency });
        })
    }

    render() {
        return (
            <Content>
                <Form>
                    <Item stackedLabel>
                        <Label>Basic Salary (Per annum)</Label>
                        <Input
                            value={this.state.basicSalary}
                            keyboardType="decimal-pad"
                            onChangeText={(value) => this.handleChange('basicSalary', value)}
                        />
                    </Item>
                    <Item stackedLabel>
                        <Label>Dearness Allowance (DA) received (Per annum)</Label>
                        <Input
                            value={this.state.daReceived}
                            keyboardType="decimal-pad"
                            onChangeText={(value) => this.handleChange('daReceived', value)}
                        />
                    </Item>
                    <Item stackedLabel>
                        <Label>HRA received (Per annum)</Label>
                        <Input
                            value={this.state.hraReceived}
                            keyboardType="decimal-pad"
                            onChangeText={(value) => this.handleChange('hraReceived', value)}
                        />
                    </Item>
                    <Item stackedLabel last picker>
                        <Label>Total Rent Paid (Per annum)</Label>
                        <Input
                            value={this.state.totalRentPaid}
                            keyboardType="decimal-pad"
                            onChangeText={(value) => this.handleChange('totalRentPaid', value)}
                        />
                    </Item>
                    <ListItem>
                        <CheckBox style={{marginRight: 20}} checked={this.state.liveInMetro} onPress={() => this.handleChange('liveInMetro', !this.state.liveInMetro)} />
                        <Body>
                            <Text onPress={() => this.handleChange('liveInMetro', !this.state.liveInMetro)}>Do you leave in metro city (Delhi, Mumbai, Kolkata or Chennai)?</Text>
                        </Body>
                    </ListItem>
                    <View style={{ padding: 5, marginTop: 5 }}>
                        <Button primary block onPress={() => this.calculateHRA()}>
                            <Icon name='calculator' />
                            <Text style={{ color: 'white' }}>Calculate</Text>
                        </Button>
                    </View>
                </Form>
                {this.state.result &&
                    <Card>
                        <CardItem header bordered>
                            <Text>Summery</Text>
                        </CardItem>
                        <CardItem bordered>
                            <Body>
                                <CardItem>
                                    <Text>Amount of exempted HRA</Text>
                                    <Right>
                                        <Text>{parseFloat(this.state.result.exemptedHRA).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                                    </Right>
                                </CardItem>
                                <CardItem>
                                    <Text>HRA chargeable to Tax</Text>
                                    <Right>
                                        <Text>{parseFloat(this.state.result.hraChargeableToTax).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                                    </Right>
                                </CardItem>
                            </Body>
                        </CardItem>
                    </Card>
                }
            </Content>
        );
    }
}

export default HRACalculator;