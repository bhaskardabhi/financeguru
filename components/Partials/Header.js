import React, { Component } from 'react';
import { Button, Header as HeaderBase, Left, Right, Icon, Body, Title, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Platform } from 'react-native';

class Header extends Component {
    render() {
        var displayBackButton = Platform.OS == 'ios' && this.props.title != 'Home';
        return (
            <HeaderBase>
                <Left>
                    {displayBackButton && (
                        <Button transparent onPress={() => Actions.pop()}>
                            <Icon name='arrow-back' />
                            <Text>Back</Text>
                        </Button>
                    )}
                    {!displayBackButton && (
                        <Button transparent onPress={() => this.props.openDrawer()}>
                            <Icon name='menu' />
                        </Button>
                    )}
                </Left>
                <Body>
                    <Title>{this.props.title}</Title>
                </Body>
                <Right />
            </HeaderBase>
        );
    }
}

export default Header;