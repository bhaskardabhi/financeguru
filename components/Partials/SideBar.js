import React from "react";
import { Share, StyleSheet, View } from "react-native";
import { Container, Content, Text, List, ListItem, Icon, Left, Body, Thumbnail } from "native-base";
import { Actions } from 'react-native-router-flux';

export default class SideBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contact: false,
            share: false
        };

        this.openLink = this.openLink.bind(this);
        this.changeModalVisibility = this.changeModalVisibility.bind(this);
    }

    openLink(action){
        if (action == "contact") {
            this.props.openModal("contact");
        } else if (action == "share"){
            this.onShare();
        } else {
            Actions[action]();
        }

        this.props.closeDrawer();
    };

    onShare = async () => {
        try {
            const result = await Share.share({
                message: 'I just found a awesome app download this from www.financeguru.com'
            });
        } catch (error) {
            alert(error.message);
        }
    };

    changeModalVisibility(modal, value){
        this.setState({
            [modal]: value
        });
    }

    render() {
        var items = [
            { title: 'Home', description: 'Calculate SIP Return', action: 'home', icon: 'home', icon_type: 'FontAwesome' },
            { title: 'SIP Calculator', description: 'Calculate SIP Return', action: 'sipCalculator', icon: 'piggy-bank', icon_type: 'FontAwesome5' },
            { title: 'Lumpsum Calculator', description: 'Calculate Lumpsum Return', action: 'lumpsumCalculator', icon: 'hand-holding-usd', icon_type: 'FontAwesome5' },
            { title: 'Fixed Deposit Calculator', description: 'Calculate Fixed Deposit Return', action: 'fdCalculator', icon: 'umbrella', icon_type: 'MaterialCommunityIcons' },
            { title: 'Recurring Deposit Calculator', description: 'Calculate Recurring Deposit Return', action: 'rdCalculator', icon: 'coins', icon_type: 'FontAwesome5' },
            { title: 'Loan Calculator', description: 'Calculate EMI, Affordability, Tenure & Interest Rate', action: 'loanCalculator', icon: 'bank', icon_type: 'AntDesign' },
            { title: 'Setting', description: 'Calculate EMI, Affordability, Tenure & Interest Rate', action: 'settings', icon: 'setting', icon_type: 'AntDesign' },
            { title: 'Contact Us', description: 'Calculate EMI, Affordability, Tenure & Interest Rate', action: 'contact', icon: 'customerservice', icon_type: 'AntDesign' },
            // { title: 'Share App', description: 'Calculate EMI, Affordability, Tenure & Interest Rate', action: 'share', icon: 'share-alt', icon_type: 'FontAwesome5' },
        ];

        return (
            <Container>
                <Content>
                    <View style={{ justifyContent: "center", alignItems: "center" }}>
                        <Thumbnail style={{ marginBottom: 10, marginTop: 10 }} source={require('./../../assets/financeGuru.png')} />
                    </View>
                    <List
                        dataArray={items}
                        renderRow={data => {
                            return (
                                <ListItem
                                    button
                                    onPress={() => this.openLink(data.action)}
                                    icon>
                                    <Left>
                                        <Icon style={{ fontSize: 20 }} type={data.icon_type} name={data.icon} />
                                    </Left>
                                    <Body>
                                        <Text>{data.title}</Text>
                                    </Body>
                                </ListItem>
                            );
                        }}
                    />
                </Content>
            </Container>
        );
    }
}