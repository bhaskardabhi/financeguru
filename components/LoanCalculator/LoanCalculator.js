import React, { Component } from 'react';
import { Text, View, AsyncStorage } from 'react-native';
import { CardItem, List, ListItem, Body, Right, Content, Card, Form, Item, Label, Input, Button, Icon, Picker } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import EMICalculationService from '../Services/EMICalculationService';
import Number from '../Services/Number';

class LoanCalculator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            periodType: "year",
            amount: null,
            percentage: null,
            period: null,
            result: null,
            currency: null
        };

        this.handleChange = this.handleChange.bind(this);
        this.calculateEMI = this.calculateEMI.bind(this);
        this.formatAmount = this.formatAmount.bind(this);
    }

    handleChange(property, value) {
        this.setState({
            [property]: value
        });
    }

    formatAmount(amount) {
        return (new Number).format(amount, this.state.currency);
    }

    calculateEMI() {
        result = (new EMICalculationService).calculateEMI(
            this.state.amount,
            this.state.percentage,
            this.state.period,
            this.state.periodType
        );

        this.setState({
            result: result
        });
    }

    componentDidMount() {
        var that = this;
        AsyncStorage.getItem('@financeGuru:currency').then((currency) => {
            that.setState({ currency: currency });
        })
    }

    render() {
        return (
            <Content>
                <Form>
                    <Item stackedLabel>
                        <Label>Loan Amount</Label>
                        <Input
                            value={this.state.amount}
                            keyboardType="decimal-pad"
                            onChangeText={(value) => this.handleChange('amount', value)}
                        />
                    </Item>
                    <Item stackedLabel>
                        <Label>Interest Rate (%)</Label>
                        <Input
                            value={this.state.percentage}
                            keyboardType="decimal-pad"
                            onChangeText={(value) => this.handleChange('percentage', value)}
                        />
                    </Item>
                    <Item stackedLabel>
                        <Label>Loan Tenure</Label>
                        <Input
                            value={this.state.period}
                            keyboardType="decimal-pad"
                            onChangeText={(value) => this.handleChange('period', value)}
                        />
                    </Item>
                    <Item stackedLabel last picker>
                        <Label>Tenure Type</Label>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: '100%' }}
                            placeholder="Tenure Type"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.periodType}
                            onValueChange={(value) => this.handleChange('periodType', value)}
                        >
                            <Picker.Item label="Year" value="year" />
                            <Picker.Item label="Month" value="month" />
                        </Picker>
                    </Item>
                    <View style={{ padding: 5, marginTop: 5 }}>
                        <Button primary block onPress={() => this.calculateEMI()}>
                            <Icon name='calculator' />
                            <Text style={{ color: 'white' }}>Calculate</Text>
                        </Button>
                    </View>
                </Form>
                {this.state.result && 
                    <Card>
                        <CardItem header bordered>
                            <Text>Summery</Text>
                        </CardItem>
                        <CardItem bordered>
                            <Body>
                                <CardItem>
                                    <Text>Monthly EMI</Text>
                                    <Right>
                                        <Text>{parseInt(this.formatAmount(this.state.result.emi))}</Text>
                                    </Right>
                                </CardItem>
                                <CardItem>
                                    <Text>Principal Amount</Text>
                                    <Right>
                                        <Text>{this.formatAmount(this.state.result.principalAmount)}</Text>
                                    </Right>
                                </CardItem>
                                <CardItem>
                                    <Text>Interest Amount</Text>
                                    <Right>
                                        <Text>{this.formatAmount(this.state.result.interestAmount)}</Text>
                                    </Right>
                                </CardItem>
                                <CardItem>
                                    <Text>Total Amount Payable</Text>
                                    <Right>
                                        <Text>{this.formatAmount(this.state.result.totalPayableAmount)}</Text>
                                    </Right>
                                </CardItem>
                            </Body>
                        </CardItem>
                    </Card>
                }
            </Content>
        );
    }
}

export default LoanCalculator;