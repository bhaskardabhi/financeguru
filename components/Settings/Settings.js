import React, { Component } from 'react';
import { Text, View, AsyncStorage } from 'react-native';
import { Content, Form, Item, Label, Icon, Picker } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

class Settings extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currency: null
        };

        AsyncStorage.getItem('@financeGuru:currency', (err, currency) => {
            this.setState({
                currency: currency ? currency : "rupee"
            });
        });
    }

    currencyChange(value) {
        this.setState({
            currency: value
        });

        AsyncStorage.setItem('@financeGuru:currency', value);
    }

    render() {
        return (
            <Content padder>
                <Form>
                    <Item picker>
                        <Label>Currency:</Label>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: '100%' }}
                            placeholder="Select Currency"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.currency}
                            onValueChange={this.currencyChange.bind(this)}>
                            <Picker.Item label="Rupee" value="rupee" />
                            <Picker.Item label="Dollar" value="dollar" />
                            <Picker.Item label="Pound" value="pound" />
                            <Picker.Item label="Euro" value="euro" />
                        </Picker>
                    </Item>
                </Form>
            </Content>
        );
    }
}

export default Settings;