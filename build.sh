#!/bin/bash

if [ -z "$1" ]
    then
        /usr/local/Cellar/node/12.3.1/bin/react-native bundle --entry-file index.js --platform android --dev false --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res
        cd android/
        ./gradlew app:assembleRelease
    else 
        /usr/local/Cellar/node/12.3.1/bin/react-native bundle --entry-file index.js --platform ios --dev false --bundle-output ios/main.jsbundle --assets-dest ios
fi