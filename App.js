import React, { Component } from 'react';
import { View, AsyncStorage, Text, StyleSheet, Linking } from 'react-native';
import { Container, Content, Drawer, Button } from 'native-base';
import Header from "./components/Partials/Header";
import SideBar from "./components/Partials/SideBar";
import Home from "./components/Home/Home";
import SipCalculator from "./components/SipCalculator/SipCalculator";
import LumpsumCalculator from "./components/LumpsumCalculator/LumpsumCalculator";
import FdCalculator from "./components/FdCalculator/FdCalculator";
import RdCalculator from "./components/RdCalculator/RdCalculator";
import LoanCalculator from "./components/LoanCalculator/LoanCalculator";
import HRACalculator from "./components/HRACalculator/HRACalculator";
import Settings from "./components/Settings/Settings";
import { Scene, Router, Stack, Reducer, Actions } from 'react-native-router-flux';
import Modal from 'react-native-modalbox';
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob';


export default class App extends Component {
  closeDrawer = () => {
    this.drawer._root.close();
  };

  openDrawer = () => {
    this.drawer._root.open();
  };

  openModal = (modal) => {
    this.refs[modal].open();
  };

  render() {
    const styles = StyleSheet.create({
      wrapper: {
        paddingTop: 50,
        flex: 1
      },
      modal: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20
      },
      modal3: {
        height: 200,
        width: 300
      },
      btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
      },
      btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
      },
      text: {
        color: "black",
        fontSize: 22
      }
    });

    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        tapToClose={true}
        content={<SideBar navigator={this.navigator} openModal={this.openModal} closeDrawer={this.closeDrawer} />}
        onClose={() => this.closeDrawer()}>
        <Container>
          <Router>
            <Stack key="root">
              <Scene initial openDrawer={this.openDrawer} navBar={Header} title="Home" component={Home} key="home" />
              <Scene openDrawer={this.openDrawer} navBar={Header} title="SIP Calculator" component={SipCalculator} key="sipCalculator" />
              <Scene openDrawer={this.openDrawer} navBar={Header} title="Lumpsum Calculator" component={LumpsumCalculator} key="lumpsumCalculator" />
              <Scene openDrawer={this.openDrawer} navBar={Header} title="FD Calculator" component={FdCalculator} key="fdCalculator" />
              <Scene openDrawer={this.openDrawer} navBar={Header} title="RD Calculator" component={RdCalculator} key="rdCalculator" />
              <Scene openDrawer={this.openDrawer} navBar={Header} title="Loan Calculator" component={LoanCalculator} key="loanCalculator" />
              <Scene openDrawer={this.openDrawer} navBar={Header} title="HRA Calculator" component={HRACalculator} key="hraCalculator" />
              <Scene openDrawer={this.openDrawer} navBar={Header} title="Settings" component={Settings} key="settings" />
            </Stack>
          </Router>
          <AdMobBanner
            adSize="fullBanner"
            adUnitID="ca-app-pub-8744220297431742/1964832560"
            testDevices={[AdMobBanner.simulatorId]}
            onAdFailedToLoad={error => console.error(error)}
          />
          <Modal style={[styles.modal, styles.modal3]} position={"center"} ref={"contact"}>
            <Text>Contact us at following Email Address:</Text>
            <Text onPress={() => Linking.openURL('mailto://dabhibhaskar2@gmail.com')}>dabhibhaskar2@gmail.com</Text>
          </Modal>
          <Modal style={[styles.modal, styles.modal3]} position={"center"} ref={"share"}>
            <Text>Share our apps:</Text>
            <Text onPress={() => Linking.openURL('www.google.com')}>www.financeGuru.com</Text>
          </Modal>
        </Container>
      </Drawer>
    );
  }
}